# **GOES-16 Python Script Docker Image**
This Docker image installs the dependencies necessary for executing the download_goes16.py Python script which interfaces with the NOAA GOES-16 S3 bucket on the AWS Registry of Open Data.

## Prerequisites
    •	Docker

## Build the Docker Image
1.	Create a Dockerfile in the root directory of your project.
2.	In the Dockerfile, specify ubuntu:22.04 as the base image:
```
FROM ubuntu:22.04
```

3. Install the necessary dependencies for running the Python script. 
The script requires python3 and the packages listed in the requirements.txt file. You can install these dependencies using the apt-get and pip3 commands:

```markdown
RUN apt-get update && apt-get install -y python3 python3-pip && pip3 install -r requirements.txt
```

4. Copy the source files and the test files from the host machine to the Docker image. You can do this using the COPY command:
```markdown
COPY . /app
```

5. Set the working directory to the /app directory:
```markdown
WORKDIR /app
```

6. Add an entry point to the Docker image. This entry point will run the download_goes16.py script when the Docker container is started. You can do this using the CMD command:
```markdown
CMD ["python3", "src/download_goes16.py", "ABI-L2-ACHAC/2022/327/17/OR_ABI-L2-ACHAC-M6_G16_s20223271736175_e20223271738548_c20223271741416.nc", "."]
```


7. Save the Dockerfile and close it.
To build the Docker image, open a terminal and navigate to the root directory of your project. Then, run the following command:
```markdown
docker build -t goes16 .
```

8. Run the Docker Container
```markdown
docker run -it goes16
```
10. The script will download the file specified in the CMD command and save it to the current directory. You can verify that the file was downloaded by running the ls command: 
```markdown
ls
```

11. To stop the Docker container, use the ```CTRL + C``` keyboard shortcut. To remove the Docker container, use the `docker rm <container ID>` command and specify the container ID:


## **Pushing the code to GitLab and writing a .gitlab-ci file to automate the testing, building, and pushing of the Docker image:**

## Prerequisites
- A GitLab account and repository set up for the project
- A GitLab shared Runner selected or gitlab runner installed on local device and registered for the repository

## Push the Code to GitLab
Initialize a local Git repository for the project and add the GitLab repository as a remote for the local repository. Then, push the code to the GitLab repository.
```python
git init
git add .
git commit -m "Initial commit"
git remote add origin <GitLab repository URL>
git push -u origin master
```

## Write the .gitlab-ci.yml file
In the root directory of the project on your local machine, create a file named `.gitlab-ci.yml.` In this file, specify the stages of the CI/CD pipeline and the jobs that will run in each stage. The stages of the CI/CD pipeline are:

```python
# Define the stages of the pipeline
stages:
  - test
  - build
  - push

# Use the ubuntu:22.04 image as the base image for the test job
test:
  stage: test
  image: ubuntu:22.04
  before_script:
  - apt-get update
  - apt-get install -y python3-pip
  - pip3 install pytest
  - pip3 install -r requirements.txt
  
  script:
    - pytest tests/

# Use the Docker image as the base image for the build job
build:
  stage: build
  image: docker:latest
  # Add the Docker service to the pipeline
  services:
    - docker:dind
  script:
    - docker build -t goes16 .

# Use the docker image as the base image for the push job
push:
  stage: push
  image: ubuntu:22.04
  # Add the Docker service to the pipeline
  services:
    - docker:dind
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker tag goes16 $CI_REGISTRY_IMAGE:latest
    - docker push $CI_REGISTRY_IMAGE:latest

```

The build and push jobs use the docker:latest image as the base image and add the docker:dind service to the pipeline. The docker:dind service is a special service that provides a Docker daemon for the pipeline to use. This allows the pipeline to execute Docker commands, such as docker build and docker push, without having to install Docker on the pipeline environment.

By using the docker:latest image and the docker:dind service, you can remove the dependencies for Docker from the .gitlab-ci.yml file and still use Docker in the pipeline to build and push the Docker image.

Note: The docker:dind service is provided by Docker and is intended for development and testing purposes only. It is not recommended for use in production environments.

# How to:
## Run the script:
```
python src/download_goes16.py ABI-L2-ACHAC/2022/327/17/OR_ABI-L2-ACHAC-M6_G16_s20223271736175_e20223271738548_c20223271741416.nc .
```

## Run PyTest:
```
pytest tests/
```

